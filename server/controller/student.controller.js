const mongoose = require('mongoose'),
Student=mongoose.model('Student'),
Result=mongoose.model('Result')

const getname = function(req,res,next){
    
  sem = req.params.sem
  Sem = sem.replace("-","/");
  console.log(Sem);
  Student.find({'Result_ID.Sem' : Sem},{name:1,Result_ID:{$elemMatch:{Sem:Sem}}})
  .then((result) => {
    if(!result)
    {
      console.log("not found");
      return res.status(200).json("NO RECORD");
    }
      else{
      console.log(result);
      return res.status(200).json({ student: result });
      
      }
  
  });
    
}

const getresult = function(req,res){
    
    _id=req.params.id
    rid=req.params.rid    
    console.log("student id : ",_id);
    console.log("Result : ",rid);

    Student.findOne({"_id":Object(_id)})
        .then((result)=>{

            var result_length = result._doc.Result_ID.length
            var R_Sem = {};
            for(i=0 ; i<result_length; i++)
            {
            if (result._doc.Result_ID[i].Id==rid)
            {
                var s = result._doc.Result_ID[i].Sem;
                var ses = s.substring(0,s.indexOf('-'));
                var sem = s.substring((s.indexOf('-')+1));
                R_Sem.ses=ses;
                R_Sem.sem=sem;
            }
            }
            console.log("Found");
            Result.findOne({_id : rid})
            .exec()
            .then((doc) => {
                console.log(R_Sem);           
                res.status(200).json({student:result,coursedata:doc,sem : R_Sem})
            })
        })
}

module.exports = {
    getname,
    getresult
}